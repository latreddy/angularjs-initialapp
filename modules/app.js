'use strict';
/*
 * AngularJS
 */
(function() {
  "use strict";
  /**
   * Initiating the app
   */
  angular.module('MyApp', [
    "app.controllers", "app.routes", "app.directive", "app.config", "app.filter", "app.services"
  ]);

  angular.module("app.routes", [
    "ui.router", "ui.bootstrap", "oc.lazyLoad", "ngAnimate"
  ]);
  angular.module("app.controllers", []);
  angular.module("app.services", []);
  angular.module("app.filter", []);
  angular.module("app.directive", []);
  angular.module("app.config", [
    "angular-loading-bar"
  ]);
  angular.module("app.factory", [
    
  ]);

  angular.module("app.config").config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
      cfpLoadingBarProvider.includeSpinner = false;
      cfpLoadingBarProvider.includeBar = true;

      toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "3000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }
      
  }]);

})();