var appRoute = angular.module("app.routes");
(function() {
    'use strict';
    appRoute.config([
        '$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider',
        function($stateProvider, $urlRouterProvider, $ocLazyLoadProvider) {

            $ocLazyLoadProvider.config({
                debug: false,
                modules: []
            });

            $urlRouterProvider.otherwise('/');

            $stateProvider.state("home", {
                name: 'home',
                url: '/',
                controller: 'homeCtrl',
                templateUrl: 'templates/home.html',
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('modules/home/home.js');
                    }]
                }
            });

            $stateProvider.state("login", {
                name: 'login',
                url: '/login',
                template: '<h3>login</h3>'
            });
        }
    ])

})();
